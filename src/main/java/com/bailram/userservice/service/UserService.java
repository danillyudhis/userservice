package com.bailram.userservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bailram.usermanagementservice.domain.dto.UserDto;
import com.bailram.usermanagementservice.domain.dto.UserRequest;
import com.bailram.usermanagementservice.domain.dto.UserResponse;

@Service
public class UserService {
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${getAllUserUrl}")
	private String getAllUserUrl;
	
	@Value("${getUserByRoleUrl}")
	private String getUserByRoleUrl;
	
	@Value("${createUser}")
	private String createUser;
	
	public UserResponse getAllUserUrl(UserRequest request) {
		return restTemplate.postForObject(getAllUserUrl, request, UserResponse.class);
	}
	
	public UserDto saveUser(UserDto user) {
		return restTemplate.postForObject(createUser, user, UserDto.class);
	}
	/*
	public String getAddUserUrl(UserDto request) {
		return restTemplate.postForObject(add, request, String.class);
	}
	*/
}
