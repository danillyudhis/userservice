package com.bailram.userservice.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bailram.usermanagementservice.domain.dto.UserDto;
import com.bailram.usermanagementservice.domain.dto.UserRequest;
import com.bailram.usermanagementservice.domain.dto.UserResponse;
import com.bailram.userservice.service.UserService;

@RestController
@RequestMapping("/v1")
public class UserServiceController {
	@Autowired
	private UserService userService;
	
	@GetMapping("/getAllUser")
	public UserResponse getAll() {
		return userService.getAllUserUrl(null);
	}
	
	@GetMapping("/getUserByRole")
	public UserResponse getByRole(@RequestBody UserRequest req) {
		return userService.getAllUserUrl(req);
	}
	
	@PostMapping
	public ResponseEntity<UserDto> saveUser(@RequestBody UserDto user) {
		try {
			UserDto insert = userService.saveUser(user);
			return new ResponseEntity<>(insert,HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
